package com.mowit.mower.dao.impl;

import org.junit.Test;

import com.mowitnow.mower.dao.impl.FileReadingServiceImpl;
import com.mowitnow.mower.exceptions.TechnicalException;

public class FileReadingServiceImplUnitTest {

	/**
	 * Test read input file when file is not found.
	 * 
	 * @throws TechnicalException
	 *             the technical exception
	 */
	@Test(expected = TechnicalException.class)
	public void testReadInputFileWhenFileNotFound() throws TechnicalException {
		FileReadingServiceImpl fileReadingService = new FileReadingServiceImpl();
		fileReadingService.setInputFilePath("aaa");
		fileReadingService.readInputFile();
	}

}
