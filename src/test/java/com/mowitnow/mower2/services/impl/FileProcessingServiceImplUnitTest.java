package com.mowitnow.mower2.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mowitnow.mower.exceptions.FunctionalException;
import com.mowitnow.mower.services.FileProcessingService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/applicationContext.xml")
public class FileProcessingServiceImplUnitTest {

	@Autowired
	private FileProcessingService fileProcessingService;

	public FileProcessingServiceImplUnitTest() {
	}

	/**
	 * Test processing lines when lines is empty.
	 * 
	 * @throws FunctionalException
	 *             the functional exception
	 */
	@Test(expected = FunctionalException.class)
	public void testProcessLinesWhenLinesIsEmpty() throws FunctionalException {
		List<String[]> lines = new ArrayList<>();
		fileProcessingService.processLines(lines);

	}

	/**
	 * Test process lines when the number of lines is incorrect.
	 * 
	 * @throws FunctionalException
	 *             the functional exception
	 */
	@Test(expected = FunctionalException.class)
	public void testProcessLinesWhenNumberOfLinesIsIncorrect()
			throws FunctionalException {
		List<String[]> lines = new ArrayList<>();
		String[] line = {"a","b","c"};
		lines.add(line);
		lines.add(line);
		lines.add(line);
		fileProcessingService.processLines(lines);
	}


}
