package com.mowitnow.mower2.launcher;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

import com.mowitnow.mower.launcher.Launcher;

public class LauncherIT {

	public LauncherIT() {
	}

	/**
	 * Test that application is not starting processing when file path is not
	 * found.
	 */
	@Test
	public void testApplicationNotStartingProcessingWhenFilePathNotFound() {
		ByteArrayOutputStream errContent = new ByteArrayOutputStream();

		System.setErr(new PrintStream(errContent));
		Launcher.applicationContextPath = "spring/applicationContext-test.xml";
		Launcher.main(null);
		assertTrue(errContent.toString().toLowerCase()
				.contains("Le fichier spécifié est introuvable".toLowerCase()));
	}

	/**
	 * Test that application is working as expected.
	 */
	@Test
	public void testApplicationWorkingAsExpected() {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		Launcher.applicationContextPath = "spring/applicationContext-test2.xml";
		Launcher.main(null);
		assertTrue(outContent
				.toString()
				.toLowerCase()
				.contains(
						"TondeusePosition [x=1, y=3, tondeuseDirection=N]"
								.toLowerCase()));
		assertTrue(outContent
				.toString()
				.toLowerCase()
				.contains(
						"TondeusePosition [x=5, y=1, tondeuseDirection=E]"
								.toLowerCase()));
	}

}
