package com.mowitnow.mower.dao;

import java.util.List;

import com.mowitnow.mower.exceptions.TechnicalException;

/**
 * The Interface FileReadingService.
 */
public interface FileReadingService {
	
	/**
	 * Read the input file.
	 * 
	 * @return the list
	 * @throws TechnicalException
	 *             the technical exception
	 */
	List<String[]> readInputFile() throws TechnicalException;

}
