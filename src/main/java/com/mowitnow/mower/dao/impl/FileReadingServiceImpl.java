package com.mowitnow.mower.dao.impl;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mowitnow.mower.dao.FileReadingService;
import com.mowitnow.mower.exceptions.TechnicalException;
import com.opencsv.CSVReader;

/**
 * The Class FileReadingServiceImpl.
 */
@Service
public class FileReadingServiceImpl implements FileReadingService {

	@Value("${input.file.path}")
	private String inputFilePath;


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mowitnow.mower.dao.FileReadingService#readInputFile()
	 */
	@Override
	public List<String[]> readInputFile() throws TechnicalException {
		CSVReader reader = null;
		try {
			if (inputFilePath != null) {
				reader = new CSVReader(new FileReader(inputFilePath));
				return reader.readAll();
			} else {
				throw new TechnicalException(
						"Le chemin du fichier dans appliction.properties est nul. Veuillez fournir une valeur valide puis ré-essayez");
			}

		} catch (FileNotFoundException e) {
			throw new TechnicalException(e);
		} catch (IOException e) {
			throw new TechnicalException(e);
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				throw new TechnicalException(e);
			}
		}
	}

	/**
	 * Sets the input file path.
	 * 
	 * @param inputFilePath
	 *            the new input file path
	 */
	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}
}
