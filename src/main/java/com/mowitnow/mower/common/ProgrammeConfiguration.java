package com.mowitnow.mower.common;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class ProgrammeConfiguration.
 */
public class ProgrammeConfiguration {

	private TaillePelouse taillePelouse;

	private List<TondeuseConfiguration> tondeuseConfigurationsInitiale;

	private List<TondeuseConfiguration> tondeuseConfigurationsFinale;

	/**
	 * Instantiates a new programme configuration.
	 * 
	 * @param taillePelouse
	 *            the taille pelouse
	 * @param tondeuseConfigurationsInitiale
	 *            the tondeuse configurations initiale
	 */
	public ProgrammeConfiguration(TaillePelouse taillePelouse,
			List<TondeuseConfiguration> tondeuseConfigurationsInitiale) {
		super();
		this.taillePelouse = taillePelouse;
		this.tondeuseConfigurationsInitiale = tondeuseConfigurationsInitiale;
		this.tondeuseConfigurationsFinale = new ArrayList<>();
	}

	/**
	 * Gets the taille of pelouse.
	 * 
	 * @return the taille pelouse
	 */
	public TaillePelouse getTaillePelouse() {
		return taillePelouse;
	}

	/**
	 * Sets the taille of the pelouse.
	 * 
	 * @param taillePelouse
	 *            the new taille pelouse
	 */
	public void setTaillePelouse(TaillePelouse taillePelouse) {
		this.taillePelouse = taillePelouse;
	}

	/**
	 * Getter de la configurations initiale.
	 * 
	 * @return the tondeuse configurations initiale
	 */
	public List<TondeuseConfiguration> getTondeuseConfigurationsInitiale() {
		return tondeuseConfigurationsInitiale;
	}

	/**
	 * Sets la configurations initiale.
	 * 
	 * @param tondeuseConfigurationsInitiale
	 *            the new tondeuse configurations initiale
	 */
	public void setTondeuseConfigurationsInitiale(
			List<TondeuseConfiguration> tondeuseConfigurationsInitiale) {
		this.tondeuseConfigurationsInitiale = tondeuseConfigurationsInitiale;
	}

	/**
	 * Getter de la configurations finale.
	 * 
	 * @return the tondeuse configurations finale
	 */
	public List<TondeuseConfiguration> getTondeuseConfigurationsFinale() {
		return tondeuseConfigurationsFinale;
	}

	/**
	 * Sets de la configurations finale.
	 * 
	 * @param tondeuseConfigurationsFinale
	 *            the new tondeuse configurations finale
	 */
	public void setTondeuseConfigurationsFinale(
			List<TondeuseConfiguration> tondeuseConfigurationsFinale) {
		this.tondeuseConfigurationsFinale = tondeuseConfigurationsFinale;
	}

}
