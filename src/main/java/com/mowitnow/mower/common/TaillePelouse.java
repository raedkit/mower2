package com.mowitnow.mower.common;

/**
 * The Class TaillePelouse.
 */
public class TaillePelouse {

	private Integer x;

	private Integer y;

	/**
	 * Instantiates a new taille pelouse.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 */
	public TaillePelouse(String x, String y) {

		this.x = Integer.valueOf(x);
		this.y = Integer.valueOf(y);
		;
	}

	/**
	 * Gets the x.
	 * 
	 * @return the x
	 */
	public Integer getX() {
		return x;
	}

	/**
	 * Sets the x.
	 * 
	 * @param x
	 *            the new x
	 */
	public void setX(Integer x) {
		this.x = x;
	}

	/**
	 * Gets the y.
	 * 
	 * @return the y
	 */
	public Integer getY() {
		return y;
	}

	/**
	 * Sets the y.
	 * 
	 * @param y
	 *            the new y
	 */
	public void setY(Integer y) {
		this.y = y;
	}

}
