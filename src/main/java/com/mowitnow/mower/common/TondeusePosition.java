package com.mowitnow.mower.common;

/**
 * The Class TondeusePosition.
 */
public class TondeusePosition {

	private Integer x;

	private Integer y;

	private Direction tondeuseDirection;

	/**
	 * Instantiates a new tondeuse position.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param direction
	 *            the direction
	 */
	public TondeusePosition(String x, String y, Direction direction) {

		this.x = Integer.valueOf(x);
		this.y = Integer.valueOf(y);
		this.tondeuseDirection = direction;
	}

	/**
	 * Gets the x.
	 * 
	 * @return the x
	 */
	public Integer getX() {
		return x;
	}

	/**
	 * Sets the x.
	 * 
	 * @param x
	 *            the new x
	 */
	public void setX(Integer x) {
		this.x = x;
	}

	/**
	 * Gets the y.
	 * 
	 * @return the y
	 */
	public Integer getY() {
		return y;
	}

	/**
	 * Sets the y.
	 * 
	 * @param y
	 *            the new y
	 */
	public void setY(Integer y) {
		this.y = y;
	}

	/**
	 * Gets the tondeuse direction.
	 * 
	 * @return the tondeuse direction
	 */
	public Direction getTondeuseDirection() {
		return tondeuseDirection;
	}

	/**
	 * Sets the tondeuse direction.
	 * 
	 * @param tondeuseDirection
	 *            the new tondeuse direction
	 */
	public void setTondeuseDirection(Direction tondeuseDirection) {
		this.tondeuseDirection = tondeuseDirection;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TondeusePosition [x=" + x + ", y=" + y + ", tondeuseDirection="
				+ tondeuseDirection + "]";
	}

}
