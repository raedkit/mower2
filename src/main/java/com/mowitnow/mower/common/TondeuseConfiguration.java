package com.mowitnow.mower.common;

/**
 * The Class TondeuseConfiguration.
 */
public class TondeuseConfiguration {

	private TondeusePosition tondeusePosition;


	private TondeuseMovements tondeuseMovements;

	/**
	 * Instantiates a new tondeuse configuration.
	 * 
	 * @param tondeusePositionAndDirection
	 *            the tondeuse position and direction
	 * @param tondeuseMovements
	 *            the tondeuse movements
	 */
	public TondeuseConfiguration(String[] tondeusePositionAndDirection,
			String[] tondeuseMovements) {
		super();
		tondeusePositionAndDirection = tondeusePositionAndDirection[0]
				.split(" ");
		this.tondeusePosition = new TondeusePosition(
				tondeusePositionAndDirection[0],
				tondeusePositionAndDirection[1],
				Direction.valueOf(tondeusePositionAndDirection[2]));
		this.tondeuseMovements = new TondeuseMovements(tondeuseMovements);
	}


	/**
	 * Gets the tondeuse position.
	 * 
	 * @return the tondeuse position
	 */
	public TondeusePosition getTondeusePosition() {
		return tondeusePosition;
	}

	/**
	 * Sets the tondeuse position.
	 * 
	 * @param tondeusePosition
	 *            the new tondeuse position
	 */
	public void setTondeusePosition(TondeusePosition tondeusePosition) {
		this.tondeusePosition = tondeusePosition;
	}

	/**
	 * Gets the tondeuse movements.
	 * 
	 * @return the tondeuse movements
	 */
	public TondeuseMovements getTondeuseMovements() {
		return tondeuseMovements;
	}

	/**
	 * Sets the tondeuse movements.
	 * 
	 * @param tondeuseMovements
	 *            the new tondeuse movements
	 */
	public void setTondeuseMovements(TondeuseMovements tondeuseMovements) {
		this.tondeuseMovements = tondeuseMovements;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TondeuseConfiguration [tondeusePosition=" + tondeusePosition
				+ ", tondeuseMovements=" + tondeuseMovements + "]";
	}

}
