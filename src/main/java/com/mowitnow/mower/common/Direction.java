package com.mowitnow.mower.common;


/**
 * The Enum Direction.
 */
public enum Direction {
	N("N"), E("E"), W("W"), S("S");
	
	private String directionValue;
	
	
	/**
	 * Instantiates a new direction.
	 * 
	 * @param value
	 *            the value
	 */
	Direction(String value) {
		this.setDirectionValue(value);
	}


	/**
	 * Gets the direction value.
	 * 
	 * @return the direction value
	 */
	public String getDirectionValue() {
		return directionValue;
	}


	/**
	 * Sets the direction value.
	 * 
	 * @param directionValue
	 *            the new direction value
	 */
	public void setDirectionValue(String directionValue) {
		this.directionValue = directionValue;
	}

	/**
	 * Checks if the direction is valid.
	 * 
	 * @param test
	 *            the test
	 * @return true, if is valid
	 */
	public static boolean isValid(Character test) {

		for (Direction direction : Direction.values()) {
			if (direction.getDirectionValue().equals(test)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Donne la nouvelle direction quand on tourne la tondeuse à gauche.
	 * 
	 * @param direction
	 *            the direction
	 * @return the direction
	 */
	public static Direction tournerGauche(Direction direction) {
		Direction resultingDirection = null;
		switch (direction) {
		case E:
			resultingDirection = Direction.N;
			break;
		case N:
			resultingDirection = Direction.W;
			break;
		case W:
			resultingDirection = Direction.S;
			break;
		case S:
			resultingDirection = Direction.E;
			break;
		default:
			break;
		}
		return resultingDirection;
	}
	
	/**
	 * Donne la nouvelle direction quand on tourne la tondeuse à droite.
	 * 
	 * @param direction
	 *            the direction
	 * @return the direction
	 */
	public static Direction tournerDroite(Direction direction) {
		Direction resultingDirection = null;
		switch (direction) {
		case E:
			resultingDirection = Direction.S;
			break;
		case N:
			resultingDirection = Direction.E;
			break;
		case W:
			resultingDirection = Direction.N;
			break;
		case S:
			resultingDirection = Direction.W;
			break;
		default:
			break;
		}
		return resultingDirection;
	}


}
