package com.mowitnow.mower.common;

/**
 * The Enum Movement.
 */
public enum Movement {
	D("D"), G("G"), A("A");

	private String movementValue;

	/**
	 * Instantiates a new movement.
	 * 
	 * @param movementValue
	 *            the movement value
	 */
	private Movement(String movementValue) {
		this.setMovementValue(movementValue);
	}

	/**
	 * Gets the movement value.
	 * 
	 * @return the movement value
	 */
	public String getMovementValue() {
		return movementValue;
	}

	/**
	 * Sets the movement value.
	 * 
	 * @param movementValue
	 *            the new movement value
	 */
	public void setMovementValue(String movementValue) {
		this.movementValue = movementValue;
	}

	/**
	 * Checks if the movement is valid.
	 * 
	 * @param test
	 *            the test
	 * @return true, if is valid
	 */
	public static boolean isValid(String test) {

		for (Movement movement : Movement.values()) {
			if (movement.getMovementValue().equals(test)) {
				return true;
			}
		}

		return false;
	}
}
