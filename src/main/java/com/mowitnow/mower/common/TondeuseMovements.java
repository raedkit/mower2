package com.mowitnow.mower.common;

import java.util.ArrayList;
import java.util.List;

import com.mowitnow.mower.util.CharacterUtil;

/**
 * The Class TondeuseMovements.
 */
public class TondeuseMovements {

	private List<Movement> movementsToDo;

	private List<Movement> movementsDone;

	private List<Movement> movementsSkipped;

	/**
	 * Instantiates a new tondeuse movements.
	 * 
	 * @param line
	 *            the line
	 */
	public TondeuseMovements(String[] line) {
		movementsToDo = new ArrayList<>();
		String movementsString = line[0];
		List<Character> tempMovementsToDo = CharacterUtil
				.stringAsList(movementsString);
		for (Character character : tempMovementsToDo) {
			movementsToDo.add(Movement.valueOf(String.valueOf(character)));
		}
		movementsDone = new ArrayList<>();
		movementsSkipped = new ArrayList<>();
	}

	/**
	 * Gets the movements to do.
	 * 
	 * @return the movements to do
	 */
	public List<Movement> getMovementsToDo() {
		return movementsToDo;
	}

	/**
	 * Sets the movements to do.
	 * 
	 * @param movementsToDo
	 *            the new movements to do
	 */
	public void setMovementsToDo(List<Movement> movementsToDo) {
		this.movementsToDo = movementsToDo;
	}

	/**
	 * Gets the movements done.
	 * 
	 * @return the movements done
	 */
	public List<Movement> getMovementsDone() {
		return movementsDone;
	}

	/**
	 * Sets the movements done.
	 * 
	 * @param movementsDone
	 *            the new movements done
	 */
	public void setMovementsDone(List<Movement> movementsDone) {
		this.movementsDone = movementsDone;
	}

	/**
	 * Gets the movements skipped.
	 * 
	 * @return the movements skipped
	 */
	public List<Movement> getMovementsSkipped() {
		return movementsSkipped;
	}

	/**
	 * Sets the movements skipped.
	 * 
	 * @param movementsSkipped
	 *            the new movements skipped
	 */
	public void setMovementsSkipped(List<Movement> movementsSkipped) {
		this.movementsSkipped = movementsSkipped;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TondeuseMovements [movementsToDo=" + movementsToDo
				+ ", movementsDone=" + movementsDone + "]";
	}


}
