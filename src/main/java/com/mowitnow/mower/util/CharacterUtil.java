package com.mowitnow.mower.util;

import java.util.AbstractList;
import java.util.List;

/**
 * The Class CharacterUtil.
 */
public class CharacterUtil {

	private CharacterUtil() {
	}

	/**
	 * String as list.
	 * 
	 * @param string
	 *            the string
	 * @return the list
	 */
	public static List<Character> stringAsList(final String string) {
		return new AbstractList<Character>() {
			public int size() {
				return string.length();
			}

			public Character get(int index) {
				return string.charAt(index);
			}
		};
	}

}
