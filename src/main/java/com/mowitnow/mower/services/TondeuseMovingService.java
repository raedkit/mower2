package com.mowitnow.mower.services;

import com.mowitnow.mower.common.TaillePelouse;
import com.mowitnow.mower.common.TondeuseConfiguration;

public interface TondeuseMovingService {

	/**
	 * Move tondeuse.
	 * 
	 * @param taillePelouse
	 *            the taille pelouse
	 * @param tondeuseConfiguration
	 *            the tondeuse configuration
	 * @return the tondeuse configuration
	 */
	public TondeuseConfiguration moveTondeuse(TaillePelouse taillePelouse,
			TondeuseConfiguration tondeuseConfiguration);

}
