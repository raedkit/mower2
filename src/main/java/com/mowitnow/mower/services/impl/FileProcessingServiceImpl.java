package com.mowitnow.mower.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.mowitnow.mower.common.ProgrammeConfiguration;
import com.mowitnow.mower.common.TaillePelouse;
import com.mowitnow.mower.common.TondeuseConfiguration;
import com.mowitnow.mower.exceptions.FunctionalException;
import com.mowitnow.mower.services.FileProcessingService;

@Service
public class FileProcessingServiceImpl implements FileProcessingService {

	private Logger LOG = LoggerFactory
			.getLogger(FileProcessingServiceImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mowitnow.mower.services.FileProcessingService#processLines(java.util
	 * .List)
	 */
	@Override
	public ProgrammeConfiguration processLines(List<String[]> lines)
			throws FunctionalException {
		List<TondeuseConfiguration> tondeuseConfigurations = new ArrayList<>();
		ProgrammeConfiguration configuration = null;
		// Test du nombre de lignes dans le fichier
		if (lines.size() % 2 != 0) {
			// Test du format de la taille de la pelouse
			String[] firstLine = lines.get(0);
			String[] taillePelouse = firstLine[0].split(" ");
			if (taillePelouse.length == 2) {
				
				TaillePelouse pelouse = new TaillePelouse(taillePelouse[0], taillePelouse[1]);
				// Création des différentes configurations des tondeuses
				for (int i = 1; i < lines.size(); i += 2) {
					tondeuseConfigurations.add(new TondeuseConfiguration(lines
							.get(i), lines.get(i + 1)));
				}
				LOG.info("Configuration initiale : "
						+ tondeuseConfigurations.toString());
				configuration = new ProgrammeConfiguration(
						pelouse, tondeuseConfigurations);
			} else {
				throw new FunctionalException(
						"Veuillez vérifier le format de la première ligne dans le fichier. La taille de la pelouse doit être composée de 2 paramètres séparés par un espce dans le fichier");

			}
		} else {
			throw new FunctionalException(
					"Le nombre de lignes contenu dans le fichier est incorrect. Le nombre de lignes doit être impair");
		}
		return configuration;
	}

}
