package com.mowitnow.mower.services.impl;

import org.springframework.stereotype.Service;

import com.mowitnow.mower.common.Direction;
import com.mowitnow.mower.common.Movement;
import com.mowitnow.mower.common.TaillePelouse;
import com.mowitnow.mower.common.TondeuseConfiguration;
import com.mowitnow.mower.common.TondeuseMovements;
import com.mowitnow.mower.common.TondeusePosition;
import com.mowitnow.mower.exceptions.FunctionalException;
import com.mowitnow.mower.services.TondeuseMovingService;

@Service
public class TondeuseMovingServiceImpl implements TondeuseMovingService {

	public TondeuseMovingServiceImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mowitnow.mower.services.TondeuseMovingService#moveTondeuse(com.mowitnow
	 * .mower.common.TaillePelouse,
	 * com.mowitnow.mower.common.TondeuseConfiguration)
	 */
	@Override
	public TondeuseConfiguration moveTondeuse(TaillePelouse taillePelouse,
			TondeuseConfiguration configuration) {
		TondeusePosition position = configuration.getTondeusePosition();
		TondeuseMovements movements = configuration.getTondeuseMovements();
		for (Movement movement : movements.getMovementsToDo()) {
			if (Movement.G.equals(movement)) {
				position.setTondeuseDirection(Direction.tournerGauche(position
						.getTondeuseDirection()));
			} else if (Movement.D.equals(movement)) {
				position.setTondeuseDirection(Direction.tournerDroite(position
						.getTondeuseDirection()));
			} else {
				try {
					managePosition(taillePelouse,
							position.getTondeuseDirection(), position);
				} catch (FunctionalException e) {
					movements.getMovementsSkipped().add(movement);
				}
			}
			movements.getMovementsDone().add(movement);
		}
		movements.getMovementsToDo().removeAll(movements.getMovementsDone());
		movements.getMovementsToDo().removeAll(movements.getMovementsSkipped());

		return configuration;
	}

	/**
	 * Manage position.
	 * 
	 * @param taillePelouse
	 *            the taille pelouse
	 * @param direction
	 *            the direction
	 * @param position
	 *            the position
	 * @throws FunctionalException
	 *             the functional exception
	 */
	private void managePosition(TaillePelouse taillePelouse,
			Direction direction, TondeusePosition position)
			throws FunctionalException {
		switch (direction) {
		case E:
			if (position.getX() + 1 <= taillePelouse.getX()) {
				position.setX(position.getX() + 1);
			} else {
				throw new FunctionalException(
						"Le mouvement ne peut pas être effectué");
			}
			break;
		case N:
			if (position.getY() + 1 <= taillePelouse.getY()) {
				position.setY(position.getY() + 1);
			} else {
				throw new FunctionalException(
						"Le mouvement ne peut pas être effectué");
			}
			break;
		case W:
			if (position.getX() - 1 >= 0) {
				position.setX(position.getX() - 1);
			} else {
				throw new FunctionalException(
						"Le mouvement ne peut pas être effectué");
			}
			break;
		case S:
			if (position.getY() - 1 >= 0) {
				position.setY(position.getY() - 1);
			} else {
				throw new FunctionalException(
						"Le mouvement ne peut pas être effectué");
			}
			break;

		default:
			break;
		}

	}

}
