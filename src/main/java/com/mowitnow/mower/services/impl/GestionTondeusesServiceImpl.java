package com.mowitnow.mower.services.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mowitnow.mower.common.ProgrammeConfiguration;
import com.mowitnow.mower.common.TaillePelouse;
import com.mowitnow.mower.common.TondeuseConfiguration;
import com.mowitnow.mower.dao.FileReadingService;
import com.mowitnow.mower.exceptions.FunctionalException;
import com.mowitnow.mower.exceptions.TechnicalException;
import com.mowitnow.mower.services.FileProcessingService;
import com.mowitnow.mower.services.GestionTondeusesService;
import com.mowitnow.mower.services.TondeuseMovingService;

@Service
public class GestionTondeusesServiceImpl implements GestionTondeusesService {

	private Log LOG = LogFactory.getLog(GestionTondeusesServiceImpl.class);

	@Autowired
	private FileProcessingService fileProcessingService;

	@Autowired
	private FileReadingService fileReadingService;

	@Autowired
	private TondeuseMovingService tondeuseMovingService;

	/**
	 * Instantiates a new gestion tondeuses service impl.
	 */
	public GestionTondeusesServiceImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mowitnow.mower.services.GestionTondeusesService#activateTondeuses()
	 */
	@Override
	public void activateTondeuses()
			throws TechnicalException, FunctionalException {
		List<String[]> inputConfiguration = fileReadingService.readInputFile();
		ProgrammeConfiguration configurationInitiale = fileProcessingService
				.processLines(inputConfiguration);
		TaillePelouse taillePelouse = configurationInitiale.getTaillePelouse();
		for (TondeuseConfiguration configuration : configurationInitiale
				.getTondeuseConfigurationsInitiale()) {
			TondeuseConfiguration tondeuseConfiguration = tondeuseMovingService
					.moveTondeuse(
					taillePelouse, configuration);
			LOG.info("La position de la tondeuse est : "
					+ tondeuseConfiguration.getTondeusePosition().toString());
		}
	}

}
