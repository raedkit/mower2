package com.mowitnow.mower.services;

import java.util.List;

import com.mowitnow.mower.common.ProgrammeConfiguration;
import com.mowitnow.mower.exceptions.FunctionalException;

public interface FileProcessingService {
	
	/**
	 * Process the lines.
	 * 
	 * @param lines
	 *            the lines
	 * @return the programme configuration
	 * @throws FunctionalException
	 *             the functional exception
	 */
	ProgrammeConfiguration processLines(List<String[]> lines)
			throws FunctionalException;

}
