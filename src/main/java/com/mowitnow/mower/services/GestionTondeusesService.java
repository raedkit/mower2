package com.mowitnow.mower.services;

import com.mowitnow.mower.exceptions.FunctionalException;
import com.mowitnow.mower.exceptions.TechnicalException;

public interface GestionTondeusesService {

	/**
	 * Activate the tondeuses.
	 * 
	 * @throws TechnicalException
	 *             the technical exception
	 * @throws FunctionalException
	 *             the functional exception
	 */
	public void activateTondeuses()
			throws TechnicalException, FunctionalException;

}
