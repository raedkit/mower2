package com.mowitnow.mower.exceptions;

/**
 * The Class TechnicalException.
 */
public class TechnicalException extends Exception {

	private static final long serialVersionUID = 4919236580758008123L;

	/**
	 * Instantiates a new technical exception.
	 */
	public TechnicalException() {
	}

	/**
	 * Instantiates a new technical exception.
	 * 
	 * @param arg0
	 *            the arg0
	 */
	public TechnicalException(String arg0) {
		super(arg0);
	}

	/**
	 * Instantiates a new technical exception.
	 * 
	 * @param arg0
	 *            the arg0
	 */
	public TechnicalException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * Instantiates a new technical exception.
	 * 
	 * @param arg0
	 *            the arg0
	 * @param arg1
	 *            the arg1
	 */
	public TechnicalException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * Instantiates a new technical exception.
	 * 
	 * @param arg0
	 *            the arg0
	 * @param arg1
	 *            the arg1
	 * @param arg2
	 *            the arg2
	 * @param arg3
	 *            the arg3
	 */
	public TechnicalException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

}
