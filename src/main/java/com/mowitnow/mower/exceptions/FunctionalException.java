package com.mowitnow.mower.exceptions;

/**
 * The Class FunctionalException.
 */
public class FunctionalException extends Exception {

	private static final long serialVersionUID = 1598908268159776275L;

	public FunctionalException() {
	}

	/**
	 * Instantiates a new functional exception.
	 * 
	 * @param message
	 *            the message
	 */
	public FunctionalException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new functional exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public FunctionalException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new functional exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public FunctionalException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new functional exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 * @param enableSuppression
	 *            the enable suppression
	 * @param writableStackTrace
	 *            the writable stack trace
	 */
	public FunctionalException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
