package com.mowitnow.mower.launcher;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.mowitnow.mower.exceptions.FunctionalException;
import com.mowitnow.mower.exceptions.TechnicalException;
import com.mowitnow.mower.services.GestionTondeusesService;

/**
 * The Class Launcher.
 */
@Component
public class Launcher {
	
	@Autowired
	private GestionTondeusesService gestionTondeusesService;

	public static String applicationContextPath = "spring/applicationContext.xml";

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		
		ApplicationContext context = 
	            new ClassPathXmlApplicationContext(applicationContextPath);
		Launcher launcher = context.getBean(Launcher.class);
        try {
			launcher.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Start.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void start() throws IOException {

		try {
			gestionTondeusesService.activateTondeuses();
		} catch (FunctionalException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (TechnicalException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
    }

}
